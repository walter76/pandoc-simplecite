# pandoc-simplecite

[![Conventional Commits](https://img.shields.io/badge/Conventional%20Commits-1.0.0-yellow.svg)](https://conventionalcommits.org)

A pandoc filter providing simple citation of documents for markdown files.

The filter is very basic right now. It reads a JSON list of references from the file given as first argument and uses
those for building the list of references and looking up the id to put up if some literature is referenced.

A sample JSON file can be found in [demos/demo.json](demos/demo.json).

To put a reference to a document somewhere in your markdown file use:

```
A very simple markdown file using one reference to @ref:1.
```

To make a list of references use:

```
{ :::refs }
```

## Installation Instructions

Just install from [PyPi](https://pypi.org) with:

```
pip install pandoc-simplecite
```

## Usage from the commandline

Use the following commandline to get HTML output (the default):

```
pandoc -t json -s demos/demo.md | python .\pandoc_simplecite.py demos/demo.json | pandoc -f json
```

Or

```
pandoc -t json -s demos/demo.md | pandoc-simplecite demos/demo.json | pandoc -f json
```

If you want to have docx output you can use:

```
pandoc -t json -s demos/demo.md | pandoc-simplecite demos/demo.json | pandoc -f json -o output/output.docx
```

As we need to pass the configuration file for the references to the filter, using the option `--filter` or `-F` is
currently not supported.

## Developer Notes

For building and testing the distribution I followed the [Packaging Projects Tutorial](https://packaging.python.org/tutorials/packaging-projects/).

### Commit Messages

We use [Conventional Commits in Angular Style](https://docs.google.com/document/d/1QrDFcIiPjSLDn3EL15IJygNPiHORgU1_OOAqWjiDU5Y/edit)
for our commit messages. A nice summary is in this Gist [Commit Message Conventions](https://gist.github.com/stephenparish/9941e89d80e2bc58a153).

Just a short reminder:

Allowed `<type>`

* feat (feature)
* fix (bug fix)
* docs (documentation)
* style (formatting, missing semi colons, ...)
* refactor
* test (when adding missing tests)
* chore (maintain)

### Build the Distribution

```
python setup.py sdist bdist_wheel
```

### Testing the Distribution

Upload the Distribution to the Test Repository:

```
python -m twine upload --repository-url https://test.pypi.org/legacy/ dist/*
```

Create an VirtualEnv and Install from the Test Repository:

```
virtualenv c:\test-install
C:\test-install\Scripts\activate.ps1
python -m pip install --index-url https://test.pypi.org/simple/ --no-deps pandoc-simplecite
```

After tests are finished, remove VirtualEnv:

```
deactivate
```

Afterwards directory can be deleted.

### Upload the Distribution to PyPi

```
python -m twine upload dist/*
```

### Update the Changelog

See [conventional-changelog/standard-version](https://github.com/conventional-changelog/standard-version) for details on
how to update the Changelog.