# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

#  (2019-03-26)


### Bug Fixes

* call pandoc-simplecite from commandline ([7cb791f](https://gitlab.com/walter76/pandoc-simplecite/commit/7cb791f))


### Features

* added shebang line to script ([8b7f0be](https://gitlab.com/walter76/pandoc-simplecite/commit/8b7f0be))
* first basic version with hardcoded json config file ([6e9fcba](https://gitlab.com/walter76/pandoc-simplecite/commit/6e9fcba))
* it is now possible to replace the citation with a text ([e107735](https://gitlab.com/walter76/pandoc-simplecite/commit/e107735))
* json input file is now a commandline argument ([64375f6](https://gitlab.com/walter76/pandoc-simplecite/commit/64375f6))
* setup and packaging for distribution ([4b42720](https://gitlab.com/walter76/pandoc-simplecite/commit/4b42720))
